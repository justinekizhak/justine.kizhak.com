module.exports = {
  purge: ['./components/**/*.vue', './pages/**/*.vue'],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      colors: require('tailwindcss/colors'),
      fontFamily: {
        jost: "'Jost'",
        // playfair: "'Playfair Display'",
        poppins: "'Poppins'",
        majorMono: "'Major Mono Display'",
        // montserrat: 'Montserrat',
        'ocean-rush': "'OceanRush'",
        ultra: 'Ultra',
        cormorant: 'Cormorant Garamond',
      },
      zIndex: {
        '-10': -10,
      },
      scale: {
        500: '5',
      },
    },
  },
  variants: {
    extend: {
      translate: ['group-hover'],
    },
  },
  plugins: [require('@tailwindcss/forms')],
}
