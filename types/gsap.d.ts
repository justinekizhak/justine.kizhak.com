// import Vue from 'vue'
import gsap from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'
import { ScrollToPlugin } from 'gsap/ScrollToPlugin'

declare module 'vue/types/vue' {
  interface Vue {
    $gsap: typeof gsap
    $ScrollTrigger: typeof ScrollTrigger
    $ScrollToPlugin: typeof ScrollToPlugin
  }
}
