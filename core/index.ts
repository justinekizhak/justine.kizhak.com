/**
 * Split the text content in an element and returns the words
 * @param element The parent whose text content is to be split
 */
export function getChildrens(element: HTMLElement) {
  if (!element || !element.textContent) {
    throw new Error('empty element')
  }
  element.innerHTML = element.textContent.replace(
    /\S+/g,
    "<span class='inline-block word'>$&</span>"
  )

  return element.querySelectorAll('.word')
}
